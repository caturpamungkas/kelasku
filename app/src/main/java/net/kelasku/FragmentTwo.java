package net.kelasku;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by tjtr.co
 */

public class FragmentTwo extends Fragment {

    ImageView pictSiswa;
    LinearLayout ttHilang, viewGroup;
    TextView namaSiswa;

    public static FragmentTwo newInstance(){
        return new FragmentTwo();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_two, container, false);
        pictSiswa = rootView.findViewById(R.id.pictSiswa);
        ttHilang = rootView.findViewById(R.id.ttHilang);
        viewGroup = rootView.findViewById(R.id.viewGroup);
        namaSiswa = rootView.findViewById(R.id.namaSiswa);

        if (getArguments() != null){
//            String keyname = getArguments().getString("key_name");
            Bundle bundle = getArguments();
            String keyname = bundle.getString("key_name");
            namaSiswa.setText(keyname);
        }

        pictSiswa.setOnClickListener(new View.OnClickListener() {
            boolean visible;

            @Override
            public void onClick(View view) {
                com.transitionseverywhere.TransitionManager.beginDelayedTransition(viewGroup);
                visible = !visible;
                ttHilang.setVisibility(visible ? View.GONE : View.VISIBLE);
            }
        });

        return rootView;
    }
}
