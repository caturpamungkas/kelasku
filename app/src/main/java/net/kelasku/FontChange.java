package net.kelasku;

import android.app.Application;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by tjtr.co on 2/5/2018.
 */

public class FontChange extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RobagaRoundedRegular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}