package net.kelasku.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by tjtr.co on 2/14/2018.
 */

public class GetPengumuman {
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("success")
    @Expose
    public Success success;
    @SerializedName("results")
    @Expose
    public List<ResultsPengumuman> results = null;
}
