package net.kelasku.Adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import net.kelasku.Models.ResultsPengumuman;
import net.kelasku.R;

import java.util.List;

/**
 * Created by tjtr.co on 2/3/17.
 */

public class PengumumanAdapter extends RecyclerView.Adapter<PengumumanAdapter.MyViewHolder>{
        private List<ResultsPengumuman> mPengumumanList;

        public PengumumanAdapter(List <ResultsPengumuman> PengumumanList) {
            mPengumumanList = PengumumanList;
        }

        @Override
        public MyViewHolder onCreateViewHolder (ViewGroup parent,int viewType){
            View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_pengumuman, parent, false);
            return new MyViewHolder(mView);
        }

        @SuppressLint({"SetTextI18n"})
        @Override
        public void onBindViewHolder (MyViewHolder holder,final int position){

            holder.tglPengumuman.setText(mPengumumanList.get(position).tanggal);
            holder.judulPengumuman.setText(mPengumumanList.get(position).title);
            holder.isiPengumuman.setText(mPengumumanList.get(position).message);

            if (mPengumumanList.get(position).important.equals(true)){
                holder.listPengumuman.setBackgroundResource(R.drawable.rounded_content_pengumuman_important);
                holder.isiPengumuman.setBackgroundResource(R.color.red_btn_ic);
            }

        }

        @Override
            public int getItemCount () {
            return mPengumumanList.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tglPengumuman, judulPengumuman, isiPengumuman;
            LinearLayout listPengumuman;

            MyViewHolder(View itemView) {
                super(itemView);
                tglPengumuman = itemView.findViewById(R.id.tglPengumuman);
                judulPengumuman = itemView.findViewById(R.id.judulPengumuman);
                isiPengumuman = itemView.findViewById(R.id.isiPengumuman);
                listPengumuman = itemView.findViewById(R.id.listPengumuman);
            }
        }
}