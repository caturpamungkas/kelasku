package net.kelasku;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.kelasku.Adapter.PengumumanAdapter;
import net.kelasku.Models.GetPengumuman;
import net.kelasku.Models.ResultsPengumuman;
import net.kelasku.api.BaseApiService;
import net.kelasku.api.UtilsApi;

import java.util.List;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPengumuman extends AppCompatActivity {

    BaseApiService mApiInterface;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    AlertDialog loading;
    @SuppressLint("StaticFieldLeak")
    public static ActivityPengumuman ma;
    SwipeRefreshLayout swLayout;
    RelativeLayout Refreshed;
    ImageView dropDown;
    TextView isiPengumuman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengumuman);
        swLayout = findViewById(R.id.swlayout);
        Refreshed = findViewById(R.id.Refreshed);
        isiPengumuman = findViewById(R.id.isiPengumuman);
        dropDown = findViewById(R.id.dropDown);

        mApiInterface = UtilsApi.getAPIService();

        mRecyclerView = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        ma=this;

        getPengumuman();

        swLayout.setColorSchemeResources(R.color.colorAccent,R.color.colorPrimary);
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swLayout.setRefreshing(false);
                        getPengumuman();
                    }
                },3000);
            }
        });
    }

    public void getPengumuman() {
        loading = new SpotsDialog(this, R.style.Custom);
        loading.show();
        Call<GetPengumuman> kontakCall = mApiInterface.getPengumuman();
        kontakCall.enqueue(new Callback<GetPengumuman>() {
            @Override
            public void onResponse(@NonNull Call<GetPengumuman> call, @NonNull Response<GetPengumuman> response) {
                loading.dismiss();
                List<ResultsPengumuman> PengumumanList = response.body().results;
                Log.d("Retrofit Get", "Jumlah data Pengumuman: " + String.valueOf(PengumumanList.size()));
                mAdapter = new PengumumanAdapter(PengumumanList);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(@NonNull Call<GetPengumuman> call, @NonNull Throwable t) {
                loading.dismiss();
                Log.e("Retrofit Get", t.toString());
            }
        });
    }
}


