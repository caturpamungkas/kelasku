package net.kelasku.api;

/**
 * Created by DESKTOP-TJTR on 1/31/2018.
 */

public class UtilsApi {

    public static final String BASE_URL_API = "https://kelasku.ga/api/";

    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
