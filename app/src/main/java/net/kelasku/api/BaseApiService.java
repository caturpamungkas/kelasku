package net.kelasku.api;

import net.kelasku.Models.GetPengumuman;
import net.kelasku.Models.ModelGetProfile;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by DESKTOP-TJTR on 1/31/2018.
 */

public interface BaseApiService {

    @FormUrlEncoded
    @POST("user/login")
    Call<ResponseBody> loginRequest(@Field("username") String username,
                                    @Field("password") String password);

    @FormUrlEncoded
    @POST("user/data")
    Call<ModelGetProfile> getData(@Field("token") String token);

    @GET("pengumuman")
    Call<GetPengumuman> getPengumuman();
}
