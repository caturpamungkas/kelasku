package net.kelasku;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.kelasku.Models.ModelGetProfile;
import net.kelasku.Utils.SharedPrefManager;
import net.kelasku.api.BaseApiService;
import net.kelasku.api.UtilsApi;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    String token, nameHasil;
    TextView namaSiswa, kelasSiswa, emailSiswa;
    Button btnMainProfile, btnMainPengumuman, btnMainTabungan;
    ImageButton btnLogout;
    BaseApiService mApiService;
    Context mContext;
    AlertDialog loading;
    ImageView pictSiswa;
    SharedPrefManager sharedPrefManager;

    ViewPager mViewPager;
    SectionsPagerAdapter mSectionsPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        namaSiswa = findViewById(R.id.namaSiswa);
        kelasSiswa = findViewById(R.id.kelasSiswa);
        emailSiswa = findViewById(R.id.emailSiswa);
        pictSiswa = findViewById(R.id.pictSiswa);
        btnMainProfile = findViewById(R.id.btnMainProfile);
        btnMainTabungan = findViewById(R.id.btnMainTabungan);
        btnMainPengumuman = findViewById(R.id.btnMainPengumuman);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.viewPagerContainer);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mApiService = UtilsApi.getAPIService();

        btnLogout = findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Anda Telah Logout", Toast.LENGTH_SHORT).show();
                sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
                startActivity(new Intent(MainActivity.this, LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });

        btnMainProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActivityProfile.class);
                startActivity(intent);
            }
        });
        btnMainTabungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Pengembangan.class);
                startActivity(intent);
            }
        });
        btnMainPengumuman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActivityPengumuman.class);
                startActivity(intent);
            }
        });

        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();
        loading = new SpotsDialog(this, R.style.Custom);
        loading.show();

        mApiService.getData(token).enqueue(new Callback<ModelGetProfile>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ModelGetProfile> call, @NonNull Response<ModelGetProfile> response) {
                try {
                    loading.dismiss();

                    nameHasil = response.body().getResults().getName();
                    String majors = response.body().getResults().getMajors();
                    String email = response.body().getResults().getEmail();
                    String urlPict = response.body().getResults().getImagedir();

                    Bundle bundle = new Bundle();
                    bundle.putString("key_name", nameHasil);
                    FragmentTwo fragmentTwo = new FragmentTwo();
                    fragmentTwo.setArguments(bundle);

//                    namaSiswa.setText(name);
//                    kelasSiswa.setText(majors);
//                    emailSiswa.setText(email);
//
//                    Picasso.with(getApplicationContext())
//                            .load(urlPict)
//                            .error(R.drawable.ic_boy)
//                            .into(pictSiswa);

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(mContext, "Pastikan Anda Terhubung Dengan Internet", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ModelGetProfile> call, @NonNull Throwable t) {
                loading.dismiss();
                Toast.makeText(mContext, "Pastikan Anda Terhubung Dengan Internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position){
                case 0:
                    fragment = FragmentTwo.newInstance();
                    break;
                case 1:
                    fragment = FragmentOne.newInstance();
                    break;
                case 2:
                    fragment = FragmentThree.newInstance();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }

}
